package kz.sunkazakh.ml.iris.classification;

import com.holdenkarau.spark.testing.JavaDatasetSuiteBase;
import org.apache.spark.sql.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DataSetManagerTest extends JavaDatasetSuiteBase
{

  @Test()
  public void shouldTransformRawData()
  {
    SQLContext     sqlContext     = new SQLContext(sc());
    SparkSession   spark          = sqlContext.sparkSession();
    DataSetManager dataSetManager = new DataSetManager(spark);

    List<TransformedDataDTO> expectedResultList = Arrays.asList(
      new TransformedDataDTO(1.0, "Iris-setosa", 1.4, 0.2, 5.0, 3.3),
      new TransformedDataDTO(2.0, "Iris-versicolor", 4.7, 1.4, 7.0, 3.2)
    );

    Dataset<Row> expectedResult = spark.<TransformedDataDTO>createDataset(
      expectedResultList,
      Encoders.bean(TransformedDataDTO.class)
    )
      .select("sepal_length", "sepal_width", "petal_length", "petal_width", "class_name", "classIndex");

    List<RawDataDTO> input = Arrays.asList(
      new RawDataDTO("5.0", "3.3", "1.4", "0.2", "Iris-setosa"),
      new RawDataDTO("7.0", "3.2", "4.7", "1.4", "Iris-versicolor")
    );

    Dataset<RawDataDTO> rawData      = spark.<RawDataDTO>createDataset(input, Encoders.bean(RawDataDTO.class));
    Dataset             actualResult = dataSetManager.transformRawData(rawData);

    // Approximate because there are DOUBLE type for numbers.
    assertDatasetApproximateEquals(actualResult, expectedResult, 0.01);
  }

  @Test(expected = AssertionError.class)
  public void shouldFailWhenDoubleInInputWithWrongSeparator()
  {
    SQLContext     sqlContext     = new SQLContext(sc());
    SparkSession   spark          = sqlContext.sparkSession();
    DataSetManager dataSetManager = new DataSetManager(spark);

    List<TransformedDataDTO> expectedResultList = Arrays.asList(
      new TransformedDataDTO(1.0, "Iris-setosa", 1.4, 0.2, 5.0, 3.3),
      new TransformedDataDTO(2.0, "Iris-versicolor", 4.7, 1.4, 7.0, 3.2)
    );

    Dataset<Row> expectedResult = spark.<TransformedDataDTO>createDataset(
      expectedResultList,
      Encoders.bean(TransformedDataDTO.class)
    )
      .select("sepal_length", "sepal_width", "petal_length", "petal_width", "class_name", "classIndex");

    List<RawDataDTO> incorrectInput = Arrays.asList(
      new RawDataDTO("5,0", "3,3", "1,4", "0,2", "Iris-setosa"),
      new RawDataDTO("7,0", "3,2", "4,7", "1,4", "Iris-versicolor")
    );

    Dataset<RawDataDTO> incorrectRawData = spark.<RawDataDTO>createDataset(
      incorrectInput,
      Encoders.bean(RawDataDTO.class)
    );
    Dataset incorrectActualResult = dataSetManager.transformRawData(incorrectRawData);

    assertDatasetApproximateEquals(incorrectActualResult, expectedResult, 0.01);
  }

  @Test
  public void shouldSplitToControlAndTestDatasets()
  {
    SQLContext     sqlContext     = new SQLContext(sc());
    SparkSession   spark          = sqlContext.sparkSession();
    DataSetManager dataSetManager = new DataSetManager(spark);

    List<TransformedDataDTO> inputDataList = new ArrayList<>();
    for (int i = 0; i < 10; i++)
    {
      inputDataList.add(new TransformedDataDTO(0.0, "Iris-virginica", 5.7, 3.4, 5.0, 1.2));
      inputDataList.add(new TransformedDataDTO(1.0, "Iris-setosa", 1.4, 0.2, 5.0, 3.3));
      inputDataList.add(new TransformedDataDTO(2.0, "Iris-versicolor", 4.7, 1.4, 7.0, 3.2));
    }

    Dataset<Row> expectedInput = spark.<TransformedDataDTO>createDataset(
      inputDataList,
      Encoders.bean(TransformedDataDTO.class)
    )
      .select("sepal_length", "sepal_width", "petal_length", "petal_width", "class_name", "classIndex");

    dataSetManager.setInputData(expectedInput);

    dataSetManager.splitToControlAndTestDatasets(expectedInput);
    Dataset<Row> testDataSet    = dataSetManager.getTestDataSet();
    Dataset<Row> controlDataSet = dataSetManager.getControlDataSet();

    //check that all data distributed between control and test groups
    assertDatasetApproximateEquals(
      testDataSet.unionAll(controlDataSet).orderBy("classIndex"),
      expectedInput.orderBy("classIndex"),
      0.01
    );
    //check that test dataset is bigger than control
    Assert.assertTrue(testDataSet.count() > controlDataSet.count());
    //check that datasets contains all classes
    Assert.assertEquals(3, testDataSet.select("classIndex").distinct().count());
    Assert.assertEquals(3, controlDataSet.select("classIndex").distinct().count());

  }
}
