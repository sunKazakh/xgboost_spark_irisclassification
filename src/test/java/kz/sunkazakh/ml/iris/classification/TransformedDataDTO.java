package kz.sunkazakh.ml.iris.classification;

import java.io.Serializable;

public class TransformedDataDTO implements Serializable
{
  private Double classIndex;
  private String class_name;
  private Double petal_length;
  private Double petal_width;
  private Double sepal_length;
  private Double sepal_width;

  public TransformedDataDTO()
  {

  }

  public TransformedDataDTO(Double classIndex, String class_name, Double petal_length, Double petal_width, Double sepal_length, Double sepal_width)
  {
    this.classIndex = classIndex;
    this.class_name = class_name;
    this.petal_length = petal_length;
    this.petal_width = petal_width;
    this.sepal_length = sepal_length;
    this.sepal_width = sepal_width;
  }

  public Double getSepal_length()
  {
    return sepal_length;
  }

  public void setSepal_length(Double sepal_length)
  {
    this.sepal_length = sepal_length;
  }

  public Double getSepal_width()
  {
    return sepal_width;
  }

  public void setSepal_width(Double sepal_width)
  {
    this.sepal_width = sepal_width;
  }

  public Double getPetal_length()
  {
    return petal_length;
  }

  public void setPetal_length(Double petal_length)
  {
    this.petal_length = petal_length;
  }

  public Double getPetal_width()
  {
    return petal_width;
  }

  public void setPetal_width(Double petal_width)
  {
    this.petal_width = petal_width;
  }

  public String getClass_name()
  {
    return class_name;
  }

  public void setClass_name(String class_name)
  {
    this.class_name = class_name;
  }

  public Double getClassIndex()
  {
    return classIndex;
  }

  public void setClassIndex(Double classIndex)
  {
    this.classIndex = classIndex;
  }
}
