package kz.sunkazakh.ml.iris.classification;

public class RawDataDTO
{
  private String _c0;
  private String _c1;
  private String _c2;
  private String _c3;
  private String _c4;

  public RawDataDTO(String _c0, String _c1, String _c2, String _c3, String _c4)
  {
    this._c0 = _c0;
    this._c1 = _c1;
    this._c2 = _c2;
    this._c3 = _c3;
    this._c4 = _c4;
  }

  public String get_c0()
  {
    return _c0;
  }

  public void set_c0(String _c0)
  {
    this._c0 = _c0;
  }

  public String get_c1()
  {
    return _c1;
  }

  public void set_c1(String _c1)
  {
    this._c1 = _c1;
  }

  public String get_c2()
  {
    return _c2;
  }

  public void set_c2(String _c2)
  {
    this._c2 = _c2;
  }

  public String get_c3()
  {
    return _c3;
  }

  public void set_c3(String _c3)
  {
    this._c3 = _c3;
  }

  public String get_c4()
  {
    return _c4;
  }

  public void set_c4(String _c4)
  {
    this._c4 = _c4;
  }
}
