package kz.sunkazakh.ml.iris.classification;

import org.apache.log4j.Logger;
import org.apache.spark.sql.AnalysisException;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.io.Serializable;
import java.util.List;

/**
 * The class organises work with input raw data.
 * It reads, makes transformation and splits data into test and control groups
 *
 * @author sunkazakh (Nazarenko Roman)
 */
class DataSetManager implements Serializable
{

  private final Logger       logger;
  private       Dataset<Row> inputData      = null;
  private       Dataset<Row> testDataSet    = null;
  private       Dataset<Row> controlDataSet = null;

  private SparkSession spark;

  // Size of test group.
  private double splitParameter = 0.6D;

  private String inputDataPath;

  DataSetManager(SparkSession spark)
  {
    logger = Logger.getLogger(this.getClass());
    this.spark = spark;
  }

  /**
   * Test group contains of data with all classes.
   * Input data are grouped by class on 3 partition.
   * Spark makes sample with splitParameter from every partition and unioins all this samples to one dataset.
   *
   * @return Dataset with test data.
   */
  Dataset<Row> getTestDataSet()
  {
    checkOrInitialiseInputData();
    return testDataSet;
  }

  /**
   * Control DataSet is input data - test data.
   *
   * @return DataSet with control data.
   */
  Dataset<Row> getControlDataSet()
  {
    checkOrInitialiseInputData();
    return controlDataSet;
  }

  private void checkOrInitialiseInputData()
  {
    if (null != inputData)
      return;

    inputDataPath = System.getProperty("inputData");
    if (null == inputDataPath)
      throw new RuntimeException("property inputData is not set");

    inputData = spark.read().csv(inputDataPath);

    inputData = transformRawData(inputData);

    //grouping by class
    inputData = inputData.repartition(inputData.col("class_name"));

    splitToControlAndTestDatasets(inputData);

    //checking size of control and test data sets
    String countQueryTemplate = "SELECT class, COUNT(*) FROM %s GROUP BY class ORDER BY class";
    logger.info("Size of test dataset = " + testDataSet.count());
    logger.info("Size of control dataset = " + controlDataSet.count());
    testDataSet.createOrReplaceTempView("testData");
    controlDataSet.createOrReplaceTempView("controlData");

    logger.info("Size of partiotins in test data set:");
    spark.sql(String.format(countQueryTemplate, "testData")).show();

    logger.info("Size of partiotins in control data set:");
    spark.sql(String.format(countQueryTemplate, "controlData")).show();
  }

  /**
   * Creating test and control datasets.
   * Data is being distributed within each class by splitParameter
   */
  void splitToControlAndTestDatasets(Dataset inputData)
  {
    inputData.createOrReplaceTempView("inputData");
    StringBuilder queryForUnion = new StringBuilder();
    String        unionTemplate = " select * from %s union all ";

    List<Row> classes = inputData.select(inputData.col("class_name")).distinct().collectAsList();
    classes.forEach(row -> {
      String uniqueClass = row.getString(0);
      try
      {
        spark.sql("select * from inputData where class_name = " + '\"' + uniqueClass + '\"')
             .sample(splitParameter)
             .createTempView("testSet" + uniqueClass.replace("-", "_"));
      }
      catch (AnalysisException e)
      {
        throw new RuntimeException("Some problems with testsets generation", e);
      }
      queryForUnion.append(String.format(unionTemplate, "testSet" + uniqueClass.replace("-", "_")));
    });

    String queryForUnionTestSets = queryForUnion.substring(0, queryForUnion.lastIndexOf("union all"));
    testDataSet = spark.sql(queryForUnionTestSets);
    controlDataSet = inputData.exceptAll(testDataSet);
    testDataSet = testDataSet.repartition(testDataSet.col("class_name"));
    controlDataSet = controlDataSet.repartition(controlDataSet.col("class_name"));
    testDataSet.cache();
    controlDataSet.cache();
  }

  /**
   * Raw data transformation with casting and creating class indexing
   */
  Dataset transformRawData(Dataset inputData)
  {
    inputData.createOrReplaceTempView("rawInput");
    inputData = spark.sql("SELECT CAST(_c0 AS DOUBLE) AS sepal_length," +
                          "CAST(_c1 AS DOUBLE) AS sepal_width," +
                          "CAST(_c2 AS DOUBLE) AS petal_length," +
                          "CAST(_c3 AS DOUBLE) AS petal_width," +
                          "CAST(_c4 AS STRING) AS class_name, " +
                          "CASE " +
                          "WHEN _c4 = 'Iris-virginica'  THEN CAST(0.0 AS DOUBLE) " +
                          "WHEN _c4 = 'Iris-setosa'     THEN CAST(1.0 AS DOUBLE) " +
                          "WHEN _c4 = 'Iris-versicolor' THEN CAST(2.0 AS DOUBLE) " +
                          "END AS classIndex " +
                          "FROM rawInput");

    return inputData;
  }

  void setInputData(Dataset<Row> inputData)
  {
    this.inputData = inputData;
  }

}
