package kz.sunkazakh.ml.iris.classification;

import ml.dmlc.xgboost4j.scala.spark.XGBoostClassificationModel;
import ml.dmlc.xgboost4j.scala.spark.XGBoostClassifier;
import org.apache.log4j.Logger;
import org.apache.spark.ml.feature.VectorAssembler;
import org.apache.spark.ml.linalg.Vector;
import org.apache.spark.sql.*;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.types.DataTypes;

/**
 * The class demonstrate how to:
 * prepare dataset for predictions
 * create xgboost model
 * make distributed and row by row predictions
 */
public class IrisClassificator
{

  private static XGBoostClassificationModel xgbClassificationModel = null;
  private final  Logger                     logger;

  public IrisClassificator()
  {
    logger = Logger.getLogger(this.getClass());
  }

  public void run(SparkSession spark)
  {
    DataSetManager manager = new DataSetManager(spark);

    Dataset<Row> testDataSet = manager.getTestDataSet();

    Dataset<Row> xgbInput = transformRawDataSetToMlDataSet(testDataSet);

    XGBoostClassifier xgbClassifier = new XGBoostClassifier()
      .setEta(0.001f)
      .setMaxDepth(5)
      .setObjective("multi:softprob")
      .setNumClass(3)
      .setNumRound(5)
      .setNumWorkers(5)
      .setFeaturesCol("features")
      .setLabelCol("classIndex");

    xgbInput.createOrReplaceTempView("xgbInput");

    xgbClassificationModel = xgbClassifier.fit(xgbInput);

    logger.info("Started predictions");

    Dataset<Row> controlDataSet = transformRawDataSetToMlDataSet(manager.getControlDataSet());
    controlDataSet.createOrReplaceTempView("controlData");

    // distributed prediction
    xgbClassificationModel.transform(controlDataSet)
                          .createOrReplaceTempView("distributed_prediction_on_control_group")
    ;

    //show accuracy of model in distributed calculation
    spark.sql(" SELECT classIndex = prediction, COUNT(*) " +
              " FROM distributed_prediction_on_control_group " +
              " GROUP BY 1").show();

    //register UDF function for using row by row prediction in SQL
    registerUDF(spark);

    //show accuracy of model in row by row prediction.
    // NB! You shouldn't use row by row prediction on big datasets, because calculations will be made on 1 executor.
    spark.sql("SELECT classIndex = PREDICT(features), COUNT(*) " +
              "FROM controlData " +
              "GROUP BY 1 ")
         .show();
  }

  /**
   * It transforms dataset to dataset with 2 columns:
   * * classIndex - double value column
   * * features   - vector value column
   *
   * @param inputDataSet - dataset with raw data
   * @return Dataset with 2 columns.
   */
  private Dataset<Row> transformRawDataSetToMlDataSet(Dataset<Row> inputDataSet)
  {
    logger.info("Started dataset transforming");
    Dataset labelTransformed = inputDataSet.drop("class");

    // create feature field
    VectorAssembler vectorAssembler = new VectorAssembler().
                                                             setInputCols(new String[]{
                                                               "sepal_length",
                                                               "sepal_width",
                                                               "petal_length",
                                                               "petal_width"
                                                             }).
                                                             setOutputCol("features");
    logger.info("Dataset transformed");
    return vectorAssembler.transform(labelTransformed).select("features", "classIndex");
  }

  private void registerUDF(SparkSession spark)
  {
    UDF1 predict = (UDF1<Vector, Double>) vector -> xgbClassificationModel.predict(vector);
    spark.sqlContext().udf().register("predict", predict, DataTypes.DoubleType);
  }
}
