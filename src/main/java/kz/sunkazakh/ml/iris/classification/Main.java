package kz.sunkazakh.ml.iris.classification;

import org.apache.spark.sql.SparkSession;

public class Main
{
  private static SparkSession spark = null;

  public static void main(String[] args)
  {
    spark = SparkSession.builder().master("local[*]").getOrCreate();
    spark.sparkContext().setLogLevel("WARN");

    new IrisClassificator().run(spark);
  }
}
