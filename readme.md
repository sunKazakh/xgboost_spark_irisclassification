Example project for data classification by xgboost + spark.

The main goal of this project is to demonstrate how xgboost, spark can work together
 and how you can write unit tests for spark code using spark-testing-base(https://github.com/holdenk/spark-testing-base)

It reproduces Iris classification on the classic dataset.

Pre requirements:

0. *nix OS. (it is possible to run xgboost app on windows, but it requires a lot of extra moves)
1. python 2.7+ (it is needed for internal ml.dmlc rabbit tracker)
2. java 8
3. maven

For running programm:

* open terminal/bash and move to work directory
* mvn clean install
* java -cp "./target/dependency/*:./target/xgboost-1.0-SNAPSHOT.jar" -DinputData=./iris.data kz.sunkazakh.ml.iris.classification.Main  ./results

As result, you will see some information about control and test sets,
 tables with prediction accuracy and detailed info about prediction in ./result directory